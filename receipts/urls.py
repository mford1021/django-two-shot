from django.urls import path
from receipts.views import receipts,create_receipt

urlpatterns = [
    path("", receipts, name='home'),
    path("create/", create_receipt, name='create_receipt')
]
