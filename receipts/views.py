from django.shortcuts import render, redirect
from receipts.models import Receipt
from django.contrib.auth.decorators import login_required
from receipts.forms import CreateReceiptForm
from django.contrib.auth import authenticate, login

# Create your views here.


@login_required
def receipts(request):
    list = Receipt.objects.filter(purchaser=request.user)
    context = {
        "list": list
    }
    return render(request, "receipts/list.html", context)

@login_required
def create_receipt(request):
    if request.method == 'POST':
        form = CreateReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("/receipts/")
    else:
        form = CreateReceiptForm()
    context = {
        'form': form,
    }
    return render(request, 'receipts/create.html', context)
